import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ExtraOptions, PreloadAllModules, RouterModule } from '@angular/router';
import { MarkdownModule } from 'ngx-markdown';
import { SecurityModule } from '@dallas-casi/security';
import { FuseModule, FuseConfigModule, FuseMockApiModule} from '@dallas-casi/fuse';
import { AppComponent } from './app.component';
import { appRoutes } from './app.routing';
import { appConfig, CoreModule } from '@dallas-casi/core';
import { LayoutModule } from '@dallas-casi/layout';
import { mockApiServices } from '@dallas-casi/fake-data'

const routerConfig: ExtraOptions = {
  preloadingStrategy       : PreloadAllModules,
  scrollPositionRestoration: 'enabled'
};

@NgModule({
  declarations: [AppComponent],
  imports     : [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes, routerConfig),
    SecurityModule,

    // Fuse, FuseConfig & FuseMockAPI
    FuseModule,
    FuseConfigModule.forRoot(appConfig),
    FuseMockApiModule.forRoot(mockApiServices),

    // Core module of your application
    CoreModule,

    // Layout module of your application
    LayoutModule,

    // 3rd party modules that require global configuration via forRoot
    MarkdownModule.forRoot({})
],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
