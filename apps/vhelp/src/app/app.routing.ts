import { Route } from '@angular/router';
import { AuthGuard } from '@dallas-casi/security';
import { NoAuthGuard } from '@dallas-casi/security';
import { LayoutComponent } from '@dallas-casi/layout';
import { InitialDataResolver } from './app.resolvers';

// @formatter:off
/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
export const appRoutes: Route[] = [

    // Redirect empty path to '/example'
    {path: '', pathMatch : 'full', redirectTo: 'example'},

    // Redirect signed in user to the '/example'
    //
    // After the user signs in, the sign in page will redirect the user to the 'signed-in-redirect'
    // path. Below is another redirection for that path to redirect the user to the desired
    // location. This is a small convenience to keep all main routes together here on this file.
    {path: 'signed-in-redirect', pathMatch : 'full', redirectTo: 'example'},

    // Auth routes for guests
    {
        path: '',
        canActivate: [NoAuthGuard],
        canActivateChild: [NoAuthGuard],
        component: LayoutComponent,
        data: {
            layout: 'empty'
        },
        children: [
            // eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
            {path: 'confirmation-required', loadChildren: () => import('libs/confirmation-required/src/lib/confirmation-required.module').then(m => m.AuthConfirmationRequiredModule)},
            // eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
            {path: 'forgot-password', loadChildren: () => import('libs/forgot-password/src/lib/forgot-password.module').then(m => m.AuthForgotPasswordModule)},
            // eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
            {path: 'reset-password', loadChildren: () => import('libs/reset-password/src/lib/reset-password.module').then(m => m.AuthResetPasswordModule)},
            // eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
            {path: 'sign-in', loadChildren: () => import('libs/sign-in/src/lib/sign-in.module').then(m => m.AuthSignInModule)},
            // eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
            {path: 'sign-up', loadChildren: () => import('libs/sign-up/src/lib/sign-up.module').then(m => m.AuthSignUpModule)}
        ]
    },

    // Auth routes for authenticated users
    {
        path: '',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        component: LayoutComponent,
        data: {
            layout: 'empty'
        },
        children: [
            // eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
            {path: 'sign-out', loadChildren: () => import('libs/sign-out/src/lib/sign-out.module').then(m => m.AuthSignOutModule)},
            // eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
            {path: 'unlock-session', loadChildren: () => import('libs/unlock-session/src/lib/unlock-session.module').then(m => m.AuthUnlockSessionModule)}
        ]
    },

    // Landing routes
    {
        path: '',
        component  : LayoutComponent,
        data: {
            layout: 'empty'
        },
        children   : [
            // eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
            {path: 'home', loadChildren: () => import('libs/landing/src/lib/landing/home/home.module').then(m => m.LandingHomeModule)},
        ]
    },

    // Admin routes
    {
        path       : '',
        // canActivate: [AuthGuard],
        // canActivateChild: [AuthGuard],
        component  : LayoutComponent,
        resolve    : {
            initialData: InitialDataResolver,
        },
        children   : [
            // eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
            {path: 'example', loadChildren: () => import('libs/admin/src/lib/example.module').then(m => m.ExampleModule)},
        ]
    }
];
