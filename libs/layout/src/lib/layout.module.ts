import { NgModule } from '@angular/core';
import { EmptyLayoutModule } from './empty/empty.module';
import { CenteredLayoutModule } from './horizontal/centered';
import { EnterpriseLayoutModule } from './horizontal/enterprise';
import { MaterialLayoutModule } from './horizontal/material';
import { ModernLayoutModule } from './horizontal/modern';
import { ClassicLayoutModule } from './vertical/classic';
import { ClassyLayoutModule } from './vertical/classy';
import { CompactLayoutModule } from './vertical/compact';
import { DenseLayoutModule } from './vertical/dense';
import { FuturisticLayoutModule } from './vertical/futuristic';
import { ThinLayoutModule } from './vertical/thin';
import { SharedModule } from '@dallas-casi/shared';
import { LayoutComponent } from './layout.component';
import { SettingsModule } from './common/settings';


const layoutModules = [
  // Empty
  EmptyLayoutModule,

  // Horizontal navigation
  CenteredLayoutModule,
  EnterpriseLayoutModule,
  MaterialLayoutModule,
  ModernLayoutModule,

  // Vertical navigation
  ClassicLayoutModule,
  ClassyLayoutModule,
  CompactLayoutModule,
  DenseLayoutModule,
  FuturisticLayoutModule,
  ThinLayoutModule
];
@NgModule({
  declarations: [
      LayoutComponent
  ],
  imports     : [
      SharedModule,
      SettingsModule,
      ...layoutModules
  ],
  exports     : [
      LayoutComponent,
      ...layoutModules
  ]
})
export class LayoutModule {}
