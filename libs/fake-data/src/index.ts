import { AcademyMockApi } from './lib/apps/academy';
import { ChatMockApi } from './lib/apps/chat';
import { ContactsMockApi } from './lib/apps/contacts';
import { ECommerceInventoryMockApi } from './lib/apps/ecommerce/inventory/api';
import { FileManagerMockApi } from './lib/apps/file-manager';
import { HelpCenterMockApi } from './lib/apps/help-center';
import { MailboxMockApi } from './lib/apps/mailbox';
import { NotesMockApi } from './lib/apps/notes';
import { ScrumboardMockApi } from './lib/apps/scrumboard';
import { TasksMockApi } from './lib/apps/tasks';
import { NavigationMockApi, NotificationsMockApi, SearchMockApi, ShortcutsMockApi, UserMockApi } from './lib/common';
import { AuthMockApi } from './lib/common/auth';
import { MessagesMockApi } from './lib/common/messages';
import { AnalyticsMockApi, CryptoMockApi, FinanceMockApi, ProjectMockApi } from './lib/dashboards';
import { ActivitiesMockApi } from './lib/pages';
import { IconsMockApi } from './lib/ui';

export * from './lib/apps/academy';
export * from './lib/apps/chat/api';
export * from './lib/apps/contacts';
export * from './lib/apps/ecommerce/inventory/api';
export * from './lib/apps/file-manager';
export * from './lib/apps/help-center';
export * from './lib/apps/mailbox';
export * from './lib/apps/notes/api';
export * from './lib/apps/scrumboard/api';
export * from './lib/apps/tasks/api';
export * from './lib/common/messages';
export * from './lib/common/auth';
export * from './lib/common/navigation';
export * from './lib/common/notifications';
export * from './lib/common/search';
export * from './lib/common/search';
export * from './lib/common/shortcuts';
export * from './lib/common/user';
export * from './lib/dashboards';
export * from './lib/pages';
export * from './lib/ui';

export const mockApiServices = [
    AcademyMockApi,
    ActivitiesMockApi,
    AnalyticsMockApi,
    AuthMockApi,
    ChatMockApi,
    ContactsMockApi,
    CryptoMockApi,
    ECommerceInventoryMockApi,
    FileManagerMockApi,
    FinanceMockApi,
    HelpCenterMockApi,
    IconsMockApi,
    MailboxMockApi,
    MessagesMockApi,
    NavigationMockApi,
    NotesMockApi,
    NotificationsMockApi,
    ProjectMockApi,
    SearchMockApi,
    ScrumboardMockApi,
    ShortcutsMockApi,
    TasksMockApi,
    UserMockApi
];