export type FuseVerticalNavigationAppearance = 'default' |
    'compact' |
    'dense' |
    'thin';
