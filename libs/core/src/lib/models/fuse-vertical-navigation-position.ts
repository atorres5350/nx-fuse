export type FuseVerticalNavigationPosition = 'left' |
    'right';
