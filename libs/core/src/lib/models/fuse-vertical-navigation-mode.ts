export type FuseVerticalNavigationMode = 'over' |
    'side';
