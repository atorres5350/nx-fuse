import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'landing-home',
  templateUrl: './home.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class LandingHomeComponent {
  /**
   * Constructor
   */
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  constructor() {}
}
