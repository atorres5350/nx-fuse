export abstract class Environment {
    abstract readonly production: boolean;
    abstract readonly appUrls: {
      readonly public: string;
      readonly portal: string;
      readonly admin: string;
    };
  }