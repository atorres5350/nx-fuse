import { Component, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@dallas-casi/fuse';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'auth-confirmation-required',
  templateUrl: './confirmation-required.component.html',
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations,
})
export class AuthConfirmationRequiredComponent {
  /**
   * Constructor
   */
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  constructor() {}
}
